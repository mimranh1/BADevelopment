﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
     
public class PointDistribution : MonoBehaviour
{
    [SerializeField] private int number = 128;
    [SerializeField] private float scaling = 1;
    [SerializeField] private Vector3[] directions;

    private void OnValidate()
    {
        directions = PointsOnSphere(number);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color=Color.green;
        foreach (var point in directions)
        {
            Gizmos.DrawRay(transform.position,point*scaling);
        }
        
    }

   private Vector3[] PointsOnSphere(int n)
    {
        var upts = new List<Vector3>();
        var inc = Mathf.PI * (3 - Mathf.Sqrt(5));
        var off = 2.0f / n;
        float x = 0;
        float y = 0;
        float z = 0;
        float r = 0;
        float phi = 0;
           
        for (var k = 0; k < n; k++){
            y = k * off - 1 + (off /2);
            r = Mathf.Sqrt(1 - y * y);
            phi = k * inc;
            x = Mathf.Cos(phi) * r;
            z = Mathf.Sin(phi) * r;
               
            upts.Add(new Vector3(x, y, z));
        }
        Vector3[] pts = upts.ToArray();
        return pts;
    }
}